import 'package:flutter/material.dart';
import 'package:test_app_for_bank/core/app_global_state.dart';

import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppGlobalState().appSetUp();
  runApp(const App());
}