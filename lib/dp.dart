import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_for_bank/pages/add_card_page/bloc/add_card_bloc.dart';
import 'package:test_app_for_bank/pages/add_card_page/data/repository/local_datasource_repository.dart';

class DependenciesProvider extends StatelessWidget {
  final Widget child;

  const DependenciesProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AddCardBloc(
            LocalDataSourceRepository(),
          )..add(FetchCardsEvent()),
        ),
      ],
      child: child,
    );
  }
}
