import 'package:flutter/material.dart';
import 'package:test_app_for_bank/dp.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app_for_bank/pages/cards_page/cards_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DependenciesProvider(
      child: Sizer(
        builder: (BuildContext context, Orientation orientation,
            DeviceType deviceType) {
          return MaterialApp(
            home: CardsPage(),
          );
        },
      ),
    );
  }
}
