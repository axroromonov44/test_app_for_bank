part of 'add_card_bloc.dart';

@immutable
abstract class AddCardEvent {}

class FetchCardsEvent extends AddCardEvent {

  FetchCardsEvent();

  @override
  List<Object?> get props => [];
}

class AdddCardEvent extends AddCardEvent {
  final CardModel cardModel;

  AdddCardEvent(this.cardModel);

  @override
  List<Object?> get props => [cardModel];
}
