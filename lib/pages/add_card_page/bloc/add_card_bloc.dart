import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../data/model/card_model.dart';
import '../data/repository/local_datasource_repository.dart';
import '../data/repository/remote_datasource_repository.dart';

part 'add_card_event.dart';

part 'add_card_state.dart';

class AddCardBloc extends Bloc<AddCardEvent, AddCardState> {
  final LocalDataSourceRepository _cardRepository;
  // final RemoteDataSourceRepository _remoteDataSourceRepository;

  AddCardBloc(this._cardRepository) : super(AddCardInitial()) {
    on<FetchCardsEvent>((event, emit) {
      try {
        emit(AddCardLoading());
        final getCards = _cardRepository.getCards();
        emit(FetchCardsState(getCards));
      } catch (e) {
        if (kDebugMode) {
          print(e);
        }
      }
    });
    on<AdddCardEvent>((event, emit) {
      emit(AddCardLoading());
      _cardRepository.addCards(event.cardModel);
    });
  }
}
