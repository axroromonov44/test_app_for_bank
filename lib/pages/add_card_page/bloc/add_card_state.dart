part of 'add_card_bloc.dart';

@immutable
abstract class AddCardState {}

class AddCardInitial extends AddCardState {}

class AddCardLoading extends AddCardState {}

class FetchCardsState extends AddCardState {
  final List<dynamic> cardModel;

  FetchCardsState(this.cardModel);
  @override
  List<Object?> get props => [cardModel];
}

class AddedCardState extends AddCardState {
  final CardModel cardModel;
  AddedCardState(this.cardModel);

  @override
  List<Object?> get props => [cardModel];
}
