import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomCard extends StatelessWidget {
  final String number;
  final String expireDate;
  final String cardName;
  final String cardImg;
  final String cvc;
  final String? color;
  final Uint8List? imageFile;

  const CustomCard({
    Key? key,
    required this.expireDate,
    required this.cardName,
    required this.cardImg,
    required this.number,
    required this.cvc,
    this.color,
    this.imageFile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 22.h,
      width: double.infinity,
      child: Stack(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            color: color == 'Yellow'
                ? Colors.yellow
                : color == "Red"
                    ? Colors.red
                    : color == "Green"
                        ? Colors.green
                        : color == "Purple"
                            ? Colors.deepPurple
                            : color == "Deep Purple"
                                ? Colors.deepPurpleAccent.withOpacity(0.1)
                                : Colors.deepPurpleAccent.withOpacity(0.1),
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                children: [
                  SizedBox(
                    height: 1.h,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          'BANK CARD',
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 18),
                        ),
                        if (cardName != '')
                          Text(
                            cardName,
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 22),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        number.length != 19 ? number : number.replaceRange(5, 14, '**** ****'),
                        style: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 18),
                      )),
                  SizedBox(
                    height: 3.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Text('Expire Date'),
                          SizedBox(
                            height: 0.4.h,
                          ),
                          Text(
                            expireDate ?? '',
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      Spacer(),
                      if (cardName == 'MASTERCARD')
                        Column(
                          children: [
                            if (cardName == 'MASTERCARD' && cvc.isNotEmpty)
                              Text(
                                'CVC',
                                style: TextStyle(fontSize: 12),
                              ),
                            SizedBox(
                              height: 0.4.h,
                            ),
                            Text(
                              cardName == 'MASTERCARD' || cvc.isEmpty
                                  ? cvc
                                  : '',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      SizedBox(
                        width: 20.w,
                      ),
                      if (cardImg != '')
                        Image.asset(
                          cardImg,
                          height: 6.h,
                          width: 16.w,
                        )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
              padding: const EdgeInsets.all(4),

              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: imageFile != null
                    ? Image.memory(
                        imageFile!,
                        opacity: const AlwaysStoppedAnimation(.3),
                        width: double.infinity,
                        fit: BoxFit.cover,
                      )
                    : Image.asset(
                        'assets/card_icons/back_img.png',
                        width: double.infinity,
                        fit: BoxFit.cover,
                        opacity: const AlwaysStoppedAnimation(.3),
                      ),
              ))
        ],
      ),
    );
  }
}
