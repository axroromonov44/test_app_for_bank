import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomColorDropdown extends StatefulWidget {
  final Function(String)? color;

  const CustomColorDropdown({Key? key, this.color}) : super(key: key);

  @override
  State<CustomColorDropdown> createState() => _CustomColorDropdownState();
}

class _CustomColorDropdownState extends State<CustomColorDropdown> {
  String dropdownvalue = 'Deep Purple';

  var items = [
    'Deep Purple',
    'Purple',
    'Yellow',
    'Green',
    'Red',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 8, right: 8),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.circular(12) //<-- SEE HERE
          ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          value: dropdownvalue,
          icon: const Icon(Icons.keyboard_arrow_down),
          items: items.map((String items) {
            return DropdownMenuItem(
              value: items,
              child: Row(
                children: [
                  Container(
                    height: 24,
                    width: 24,
                    decoration: BoxDecoration(
                        color: items == 'Yellow'
                            ? Colors.yellow
                            : items == "Red"
                                ? Colors.red
                                : items == "Green"
                                    ? Colors.green
                                    : items == "Purple"
                                        ? Colors.deepPurple
                                        : items == "Deep Purple"
                                            ? Colors.deepPurpleAccent.withOpacity(0.1)
                                            : Colors.deepPurpleAccent.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(12)),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  Text(items),
                ],
              ),
            );
          }).toList(),
          hint: Text('choose'),
          onChanged: (String? newValue) {
            setState(() {
              dropdownvalue = newValue!;
              widget.color!(newValue);
            });
          },
        ),
      ),
    );
  }
}
