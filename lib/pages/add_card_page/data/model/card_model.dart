import 'dart:io';
import 'dart:typed_data';

import 'package:hive/hive.dart';

part 'card_model.g.dart';

@HiveType(typeId: 1)
class CardModel {
  @HiveField(0)
  final String number;
  @HiveField(1)
  final String expireDate;
  @HiveField(2)
  final String cardName;
  @HiveField(3)
  final String cardImg;
  @HiveField(4)
  final String? cvc;
  @HiveField(5)
  final String? color;
  @HiveField(6)
  final Uint8List? backgroundImg;

  const CardModel({
    required this.number,
    required this.cardImg,
    required this.cardName,
    required this.expireDate,
    required this.color,
    this.cvc,
    this.backgroundImg,
  });

  factory CardModel.fromJson(Map<String, dynamic> json) => CardModel(
        number: json['number'],
        cardImg: json['cardImg'],
        cardName: json['cardName'],
        expireDate: json['expireDate'],
        color: json['color'],
        cvc: json['cvc'],
        backgroundImg: json['backgroundImg'],
      );

  Map<String, dynamic> toJson() => {
        "number": number,
        "cardImg": cardImg,
        "cardName": cardName,
        "expireDate": expireDate,
        "color": color,
        "cvc": cvc,
        "backgroundImg": backgroundImg,
      };
}
