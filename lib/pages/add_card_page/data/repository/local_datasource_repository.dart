import 'package:test_app_for_bank/core/app_global_state.dart';
import 'package:test_app_for_bank/pages/add_card_page/data/model/card_model.dart';

class LocalDataSourceRepository {
  List<dynamic> getCards() {
    final cards = AppGlobalState.hive.values.toList();
    return cards;
  }

  void addCards(CardModel cardModel) {
    AppGlobalState.hive.add(CardModel(
      number: cardModel.number,
      cardImg: cardModel.cardImg,
      cardName: cardModel.cardName,
      expireDate: cardModel.expireDate,
      color: cardModel.color,
      cvc: cardModel.cvc,
      backgroundImg: cardModel.backgroundImg,
    ));
  }
}
