import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app_for_bank/core/components/custom_appbar.dart';
import 'package:test_app_for_bank/core/components/custom_btn.dart';
import 'package:test_app_for_bank/core/components/custom_textfield.dart';
import 'package:test_app_for_bank/pages/add_card_page/data/model/card_model.dart';
import 'package:test_app_for_bank/pages/add_card_page/widget/custom_card_widget.dart';
import 'package:test_app_for_bank/pages/add_card_page/widget/custom_color_dropdown.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'bloc/add_card_bloc.dart';

enum ImgSource { gallery }

class AddCardPage extends StatefulWidget {
  const AddCardPage({Key? key}) : super(key: key);

  @override
  State<AddCardPage> createState() => _AddCardPageState();
}

class _AddCardPageState extends State<AddCardPage> {
  TextEditingController? _cardNumberController,
      _cardExpireDateController,
      _cardCVCController;
  Uint8List? imageFile;
  bool isLoading = false;
  String? colorType;
  String? cardImg;
  String? cardName;

  @override
  void initState() {
    _cardExpireDateController = TextEditingController();
    _cardNumberController = TextEditingController();
    _cardCVCController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _cardNumberController?.dispose();
    _cardExpireDateController?.dispose();
    _cardCVCController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CustomAppbar(),
        body: BlocBuilder<AddCardBloc, AddCardState>(
          builder: (context, state) {
            if (state is AddCardLoading) {
              isLoading = true;
            } else if (state is AddedCardState) {
              isLoading = false;
            }
            return LoadingOverlay(
              isLoading: isLoading,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Card Preview',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      GestureDetector(
                        onTap: () {
                          _showPicker(context);
                        },
                        child: CustomCard(
                          cvc: _cardCVCController!.text == ''
                              ? 'xxx'
                              : _cardCVCController!.text,
                          expireDate: _cardExpireDateController!.text == ''
                              ? 'MM / YY'
                              : _cardExpireDateController!.text,
                          cardName: cardName ?? '',
                          cardImg: cardImg ?? '',
                          number: _cardNumberController!.text == ''
                              ? 'XXXX XXXX XXXX XXXX'
                              : _cardNumberController!.text,
                          color: colorType,
                          imageFile: imageFile,
                        ),
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      CustomTextField(
                        controller: _cardNumberController,
                        label: "Card number",
                        hintText: "Card number",
                        inputFormatters: "#### #### #### ####",
                        keyboardType: TextInputType.number,
                        onChanged: (val) {
                          if (_cardNumberController!.text.startsWith("9860")) {
                            cardImg = 'assets/card_icons/HUMO.png';
                            cardName = 'HUMO';
                          } else if (_cardNumberController!.text
                              .startsWith("8600")) {
                            cardImg = 'assets/card_icons/UZCARD.png';
                            cardName = 'UZCARD';
                          } else if (_cardNumberController!.text
                              .startsWith("5535")) {
                            cardImg = 'assets/card_icons/MASTERCARD.png';
                            cardName = 'MASTERCARD';
                          }
                          setState(() {});
                        },
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: CustomTextField(
                              controller: _cardExpireDateController,
                              width: 30.w,
                              label: "Expire date",
                              hintText: "MM / YY",
                              inputFormatters: "## / ##",
                              keyboardType: TextInputType.number,
                              onChanged: (val) => setState(() {}),
                            ),
                          ),
                          if (cardName == 'MASTERCARD')
                            Expanded(
                              child: CustomTextField(
                                controller: _cardCVCController,
                                width: 30.w,
                                label: "CVC",
                                hintText: "CVC",
                                inputFormatters: "###",
                                keyboardType: TextInputType.number,
                                onChanged: (val) => setState(() {}),
                              ),
                            ),
                        ],
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      CustomColorDropdown(
                        color: (val) {
                          colorType = val;
                          setState(() {});
                        },
                      ),
                      SizedBox(
                        height: 4.h,
                      ),
                      CustomBtn(
                        title: "Save",
                        onPressed: () {
                          if (_cardNumberController!.text.isNotEmpty &&
                              _cardExpireDateController!.text.isNotEmpty ) {
                            final cardModel = CardModel(
                              number: _cardNumberController!.text,
                              cardImg: cardImg ?? '',
                              cardName: cardName ?? '',
                              cvc: cardName == "MASTERCARD"
                                  ? _cardCVCController!.text
                                  : '',
                              expireDate: _cardExpireDateController!.text,
                              color: colorType ?? "Deep Purple",
                              backgroundImg: imageFile,
                            );
                            BlocProvider.of<AddCardBloc>(context)
                                .add(AdddCardEvent(cardModel));

                            Future.delayed(Duration(seconds: 1))
                                .then((value) => Navigator.pop(context));
                          }
                        },
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void _showPicker(context) => showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                  leading: const Icon(Icons.photo_library),
                  title: const Text('photo'),
                  onTap: () {
                    _setNewImage(ImgSource.gallery);
                    Navigator.of(context).pop();
                  }),
            ],
          ),
        );
      });

  Future<void> _setNewImage(ImgSource imageSource) async {
    XFile? image = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 25);

    File file = File(image!.path);
    setState(() {
      imageFile = file.readAsBytesSync();
    });
  }
}
