import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:test_app_for_bank/core/components/custom_appbar.dart';
import 'package:test_app_for_bank/pages/add_card_page/add_card_page.dart';
import 'package:test_app_for_bank/pages/add_card_page/widget/custom_card_widget.dart';

import '../add_card_page/bloc/add_card_bloc.dart';
import '../add_card_page/data/repository/local_datasource_repository.dart';

class CardsPage extends StatefulWidget {
  const CardsPage({Key? key}) : super(key: key);

  @override
  State<CardsPage> createState() => _CardsPageState();
}

class _CardsPageState extends State<CardsPage> {
  late AddCardBloc _addCardBloc;

  @override
  void initState() {
    _addCardBloc = AddCardBloc(LocalDataSourceRepository());
    _addCardBloc.add(FetchCardsEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppbar(
        title: 'Payment Cards',
        isHome: true,
        addIcon: BlocBuilder<AddCardBloc, AddCardState>(
          bloc: _addCardBloc,
          builder: (context, state) {
            return IconButton(
              onPressed: () async {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => BlocProvider(
                              create: (context) =>
                                  AddCardBloc(LocalDataSourceRepository()),
                              child: AddCardPage(),
                            )));
                _addCardBloc.add(FetchCardsEvent());
              },
              icon: const Icon(
                Icons.add_circle_outline,
                color: Colors.black,
                size: 32,
              ),
            );
          },
        ),
      ),
      body: BlocBuilder<AddCardBloc, AddCardState>(
        bloc: _addCardBloc..add(FetchCardsEvent()),
        builder: (context, state) {
          if (state is FetchCardsState) {
            List cardModel;
            cardModel = state.cardModel;
            return cardModel.isEmpty
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'No Cards',
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        FloatingActionButton(
                          backgroundColor: Colors.deepPurpleAccent,
                          onPressed: () async {
                            await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => BlocProvider(
                                          create: (context) => AddCardBloc(
                                              LocalDataSourceRepository()),
                                          child: AddCardPage(),
                                        ))).then((value) {
                              _addCardBloc.add(FetchCardsEvent());
                            });
                          },
                          child: Icon(
                            Icons.add_circle_outline,
                            size: 36,
                          ),
                        ),
                      ],
                    ),
                  )
                : SizedBox(
                    height: double.infinity,
                    child: ListView.builder(
                        itemCount: cardModel.length,
                        shrinkWrap: true,
                        padding: EdgeInsets.all(8.0),
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (ctx, int index) {
                          return CustomCard(
                            cardImg: cardModel[index].cardImg,
                            cardName: cardModel[index].cardName,
                            cvc: cardModel[index].cardName == 'MASTERCARD'
                                ? cardModel[index].cvc
                                : 'xxx',
                            number: cardModel[index].number,
                            expireDate: cardModel[index].expireDate,
                            color: cardModel[index].color,
                            imageFile: cardModel[index].backgroundImg,);
                        }),
                  );
          }
          return SizedBox();
        },
      ),
    );
  }
}
