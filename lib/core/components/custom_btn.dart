import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomBtn extends StatelessWidget {
  final String title;
  final bool? withOpacity;
  final GestureTapCallback? onPressed;

  const CustomBtn({
    Key? key,
    required this.title,
    this.withOpacity,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      minWidth: double.infinity,
      height: 6.h,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      color: withOpacity != null
          ? Colors.deepPurpleAccent.withOpacity(0.3)
          : Colors.deepPurpleAccent,
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w800,
          fontSize: 14
        ),
      ),
    );
  }
}
