import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_for_bank/pages/add_card_page/add_card_page.dart';

import '../../pages/add_card_page/bloc/add_card_bloc.dart';
import '../../pages/add_card_page/data/repository/local_datasource_repository.dart';

class CustomAppbar extends StatelessWidget with PreferredSizeWidget {
  final String? title;
  final Widget? addIcon;
  final bool isHome;

  CustomAppbar({Key? key, this.title, this.isHome = false, this.addIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        title ?? '',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      leading: !isHome
          ? IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ))
          : SizedBox(),
      actions: [
        if (isHome == true)
        addIcon!
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
