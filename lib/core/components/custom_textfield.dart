import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:sizer/sizer.dart';

class CustomTextField extends StatelessWidget {
  final String? label;
  final String? inputFormatters;
  final Widget? suffixIcon;
  final String? hintText;
  final double? width;
  final TextStyle? hintStyle;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final Function(String)? getValue;
  final InputBorder? border;
  final bool obscureText;

  const CustomTextField({
    Key? key,
    this.label,
    this.inputFormatters,
    this.border,
    this.suffixIcon,
    this.hintText,
    this.hintStyle,
    this.onChanged,
    this.obscureText = false,
    this.keyboardType,
    this.width,
    this.controller,
    this.getValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 8.0, left: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label ?? '',
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 2.h,
          ),
          Container(
            width: width ?? double.infinity,
            padding: EdgeInsets.only(left: 12, right: 12),
            decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.3),
                borderRadius: BorderRadius.circular(12)),
            child: TextField(
              controller: controller,
              onChanged: onChanged,
              inputFormatters: [
                MaskedInputFormatter(
                    inputFormatters ?? "###########################")
              ],
              keyboardType: keyboardType,
              decoration: InputDecoration(
                  hintText: hintText,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none),
            ),
          ),
        ],
      ),
    );
  }
}
