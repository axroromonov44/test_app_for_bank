import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

import '../app_global_state.dart';
import '../error/exception.dart';
import 'http_util.dart';
import 'network_state.dart';

abstract class ApiClient {
  ///Get Request
  static Future<dynamic> getRequest(
      String url,
      ) async =>
      _request(HttpUtil.request(url, requestType: get));

  ///Post Request
  static Future<dynamic> postRequest(
      String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: post, body: body));

  ///MultiPart Request
  static Future<dynamic> multiPartUpload(
      String uploadingDataType, String url, String filePath) async =>
      _request(HttpUtil.uploadMultiPart(uploadingDataType, url, filePath),
          isMultiPartRequest: true);

  static Future<dynamic> _request(Future<Response> request,
      {bool isMultiPartRequest = false}) async {
    if (!await NetworkState.isInternetConnected()) {
      throw const SocketException('no internet');
    }
    final Response response = await request;

    if (response.statusCode == 200 || response.statusCode == 201) {
      if (isMultiPartRequest) {
        return json.decode(utf8.decode(response.bodyBytes));
      }

      if (response.body.isNotEmpty) {
        return json.decode(utf8.decode(response.bodyBytes));
      }

      return;
    } else {
      throw ServerException(int.parse(response.statusCode.toString()));
    }
  }
}
