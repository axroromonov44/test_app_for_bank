import 'dart:convert';

import 'package:http/http.dart';
import 'package:test_app_for_bank/core/utils/print_log.dart';

import '../error/exception.dart';

abstract class HttpUtil {
  static Future<Response> request(String url,
      {Map<String, String>? headers,
        dynamic body,
        required Function requestType}) async {
    try {
      printLog(url);

      final httpHeaders = <String, String>{
        'Content-Type': 'application/json',
      };

      if (headers != null) httpHeaders.addAll(headers);

      dynamic response;

      if (requestType == get) {
        response = await requestType(Uri.parse(url), headers: httpHeaders);
      } else {

        response = await requestType(Uri.parse(url),
            headers: httpHeaders, body: jsonEncode(body));
      }

      printLog("Response body:  ${response.body}");

      return response as Response;
    } catch (e) {
      printLog(
          "Error caught in request() method inside http_utils.dart file:\n $e");
      throw const ServerException(-1);
    }
  }

  static Future<Response> uploadMultiPart(
      ///'image', 'file',
      String uploadingDataType,
      String url,
      String filePath,
      ) async {
    printLog(url);

    try {
      final request = MultipartRequest('POST', Uri.parse(url))
        ..files.add(await MultipartFile.fromPath(
          uploadingDataType,
          filePath,
        ));

      final response = await Response.fromStream(await request.send());

      printLog("Response StatusCode: ${response.statusCode}");
      printLog("Response body: ${response.body}");

      return response;
    } catch (e) {
      printLog(
          "Error caught in uploadMultiPart() method inside http_utils.dart file\n $e");

      throw const ServerException(-1);
    }
  }
}

