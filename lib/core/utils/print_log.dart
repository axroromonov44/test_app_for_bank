import 'package:flutter/foundation.dart';

void printLog(Object logText) {
  if (kDebugMode) print("$logText");
}
