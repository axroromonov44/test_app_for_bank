import 'package:hive_flutter/hive_flutter.dart';
import 'package:test_app_for_bank/pages/add_card_page/data/model/card_model.dart';

class AppGlobalState {
  static late Box hive;

  Future<void> appSetUp() async {
    await Hive.initFlutter();
    Hive.registerAdapter(CardModelAdapter());
    hive = await Hive.openBox('globalHive');
  }
}
