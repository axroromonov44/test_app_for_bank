class ServerException implements Exception {
  final int? statusCode;

  const ServerException([this.statusCode]);

  @override
  String toString() {
    return "${statusCode ?? ''}";
  }
}

class CacheException implements Exception {
  const CacheException();
}

class TokenExpiredException implements Exception {
  const TokenExpiredException();
}

